CREATE TABLE simplejdbc.Project
(
    id    INT(16) AUTO_INCREMENT NOT NULL,
    name  VARCHAR(256) NULL,
    note  TEXT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE simplejdbc.Transaction
(
    id    INT(16) AUTO_INCREMENT NOT NULL,
    fk_project_id INT NOT NULL,
    description VARCHAR(256) NULL,
    date  DATETIME NOT NULL,
    amount DOUBLE NOT NULL,
    type BOOLEAN NOT NULL,
    comment TEXT NULL,
    PRIMARY KEY (ID),
    FOREIGN KEY (fk_project_id) REFERENCES Project(id)
);

INSERT INTO simplejdbc.Project (name, note) VALUES ();

INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES ('2021-06-27','My first note', 'OMG, does it work?????');
INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES ('2019-06-28','The second note', 'Cool, looks like it works!');
INSERT INTO simplejdbc.MyJavaNote (date, title, note) VALUES ('2019-05-29','Is this the 3rd one?', 'I thought I remember it without writing it down....');
