package com.java3.mincounting.beans;

import com.java3.mincounting.helpers.Validator;
import com.java3.mincounting.models.Project;
import com.java3.mincounting.models.ProjectDatastore;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.bean.ManagedBean;
import jakarta.faces.bean.ManagedProperty;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;

import java.util.List;

@ManagedBean(name = "project")
@RequestScoped
public class ProjectBean {
    @ManagedProperty(value = "#{param.id}")
    private int objectID = 0;
    /*
     * Datastore
     */
    private ProjectDatastore db = new ProjectDatastore();

    /*
     * Project Properties
     */
    private String projectName;
    private String projectDescription;
    private Integer projectID;

    /**
     * Page Constructor
     */
    @PostConstruct
    public void postConstruct() {
        if (objectID != 0) {
            Project project = db.get(objectID);
            projectID = project.getId();
            projectName = project.getName();
            projectDescription = project.getDescription();
        }
    }

    public String createProject(String name, String description) {
        Project project = new Project(name, description);
        db.create(project);
        return "/projects.xhtml?faces-redirect=true";
    }

    public String updateProject() {
        Project project = new Project(projectID, projectName, projectDescription);
        db.update(project);
        return "/projects.xhtml?faces-redirect=true";
    }

    public String deleteProject(int id) {
        db.delete(id);
        return "/projects.xhtml?faces-redirect=true";
    }

    public void validate(FacesContext context, UIComponent comp, Object value) {
        // Rule Factory...
        switch (comp.getId()) {
            case "projectDescription":
                Validator.required(context, comp, value);
                Validator.maxLength(context, comp, value, 512);
                Validator.minLength(context, comp, value, 10);
                break;
            case "projectName":
                Validator.required(context, comp, value);
                Validator.maxLength(context, comp, value, 250);
                Validator.minLength(context, comp, value, 4);
                break;
        }
    }

    /*
     * Setters & Getters
     */
    public int getObjectID() {
        return objectID;
    }

    public Project getProject(int id) {
        return db.get(id);
    }

    public void setObjectID(int objectID) {
        this.objectID = objectID;
    }

    public List<Project> getProjectList() {
        return db.getAll();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }
}
