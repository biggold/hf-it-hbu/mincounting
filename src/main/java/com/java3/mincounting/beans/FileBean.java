package com.java3.mincounting.beans;

import com.java3.mincounting.models.Project;
import com.java3.mincounting.models.ProjectDatastore;
import com.java3.mincounting.models.Transaction;
import com.java3.mincounting.models.TransactionDatastore;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.bean.ManagedBean;
import jakarta.faces.context.FacesContext;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import jakarta.servlet.http.Part;
import org.w3c.dom.*;

import java.io.InputStream;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.List;

/**
 * Responsible for Exporting/Importing data via XML.
 *
 * @author Cyrill Näf
 */
@ManagedBean(name = "file")
@RequestScoped
public class FileBean {
    private final ProjectDatastore pdb = new ProjectDatastore();
    private final TransactionDatastore tdb = new TransactionDatastore();
    private Part uploadedFile;

    public Part getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(Part uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    /**
     * Downloads a xml file containing all notes from the database
     */
    public void download() {
        String xml = this.generateXML();
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.setContentType("text/xml");
        response.setHeader("Content-Disposition", "attachment;filename=file.xml");
        try {
            ServletOutputStream out = response.getOutputStream();
            out.write(xml.getBytes());
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Processes the uploaded xml file
     */
    public void upload() {
        try {
            this.parseXML();
        } catch (Exception e) {
            // We return a error if the xml import throws any exception
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.setStatus(500);
        }
    }

    /**
     * Parses the xml file
     *
     * @throws Exception Return any exception back to the caller. Caller should handle together with frontend
     */
    protected void parseXML() throws Exception {
        // XML Helpers....
        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        InputStream inputStream = getUploadedFile().getInputStream();
        Document document = documentBuilder.parse(inputStream);

        // Get root element
        document.getDocumentElement().normalize();

        // Get all childs of type project
        NodeList nodeList = document.getElementsByTagName("project");

        // Iterate over our childs
        for (int counter = 0; counter < nodeList.getLength(); counter++) {
            Node nNode = nodeList.item(counter);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                Integer id = null;
                try {
                    id = Integer.parseInt(eElement.getAttribute("id"));
                } catch (NumberFormatException e) {
                    id = null;
                }
                String name = eElement.getElementsByTagName("name").item(0).getTextContent();
                String description = eElement.getElementsByTagName("description").item(0).getTextContent();
                // Generate Project object from xml
                Project project = new Project(id, name, description);
                // Do update logic
                // We do not care about already existing elements
                if (id == null || pdb.get(project.getId()).isNull()) {
                    pdb.create(project);
                    project = pdb.get(project.getName(), project.getDescription());
                } else {
                    // Here we assume that the object with that ID already exists, so we update it
                    pdb.update(project);
                }

                //Process Transactions
                NodeList transactionsNodeList = eElement.getElementsByTagName("transaction");
                for (int counter2 = 0; counter2 < transactionsNodeList.getLength(); counter2++) {
                    Node tNode = transactionsNodeList.item(counter2);
                    if (tNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element tElement = (Element) tNode;

                        LocalDate tDate = LocalDate.parse(tElement.getElementsByTagName("date").item(0).getTextContent());
                        String tDescription = tElement.getElementsByTagName("description").item(0).getTextContent();
                        String tAmount = tElement.getElementsByTagName("amount").item(0).getTextContent();
                        String tComment = tElement.getElementsByTagName("comment").item(0).getTextContent();
                        // Generate Transaction object from xml
                        Transaction transaction = new Transaction(project.getId(), tDate, Double.valueOf(tAmount), tDescription, tComment);
                        // We do not care about already existing elements
                        tdb.create(transaction);
                    }
                }
            }
        }

    }

    /**
     * Magic, generates the xml as string
     *
     * @return XML String
     */
    protected String generateXML() {
        try {
            // Get all projects
            List<Project> projectList = pdb.getAll();
            // XML Helpers....
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // root element - notes
            Element root = document.createElement("projects");
            document.appendChild(root);

            // add all note's
            for (Project project : projectList) {
                // project element
                Element pElement = document.createElement("project");
                root.appendChild(pElement);

                // set an attribute to element
                Attr pAttr = document.createAttribute("id");
                pAttr.setValue(Integer.toString(project.getId()));
                pElement.setAttributeNode(pAttr);

                // name element
                Element pName = document.createElement("name");
                pName.appendChild(document.createTextNode(project.getName()));
                pElement.appendChild(pName);

                // description element
                Element pDescription = document.createElement("description");
                pDescription.appendChild(document.createTextNode(project.getDescription()));
                pElement.appendChild(pDescription);

                // transactions element
                Element pTransactions = document.createElement("transactions");
                pElement.appendChild(pTransactions);

                // Get all transactions
                List<Transaction> transactionList = tdb.getAll(project.getId());
                for (Transaction transaction : transactionList) {
                    // transaction element
                    Element tElement = document.createElement("transaction");
                    pTransactions.appendChild(tElement);

                    // date element
                    Element tDate = document.createElement("date");
                    tDate.appendChild(document.createTextNode(transaction.getDate().toString()));
                    tElement.appendChild(tDate);

                    // amount element
                    Element tAmount = document.createElement("amount");
                    tAmount.appendChild(document.createTextNode(transaction.getAmount().toString()));
                    tElement.appendChild(tAmount);

                    // description element
                    Element tDescription = document.createElement("description");
                    tDescription.appendChild(document.createTextNode(transaction.getDescription()));
                    tElement.appendChild(tDescription);

                    // comment element
                    Element tComment = document.createElement("comment");
                    tComment.appendChild(document.createTextNode(transaction.getComment()));
                    tElement.appendChild(tComment);
                }

            }
            // Transform Document to XML String
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(writer));

            // If you use
            // StreamResult result = new StreamResult(System.out);
            // the output will be pushed to the standard output ...
            // You can use that for debugging
            return writer.toString();

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
        return "";
    }
}
