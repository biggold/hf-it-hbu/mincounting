package com.java3.mincounting.beans;

import com.java3.mincounting.models.Transaction;
import com.java3.mincounting.models.TransactionDatastore;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.bean.ManagedBean;

import java.util.List;

@ManagedBean(name = "dashboard")
@RequestScoped
public class DashboardBean {
    /*
     * Datastore
     */
    private TransactionDatastore tdb = new TransactionDatastore();

    public String calculateBalance(int projectID) {
        List<Transaction> transactions = tdb.getAll(projectID);
        double balance = 0.0;
        for(Transaction transaction: transactions) {
            balance = balance + transaction.getAmount();
        }
        if (balance >= 0) {
            return "+"+Double.toString(balance);
        } else if (balance < 0) {
            return Double.toString(balance);
        }
        return Double.toString(balance);
    }

    public String calculateTotal() {
        List<Transaction> transactions = tdb.getAll();
        double total = 0.0;
        for(Transaction transaction: transactions) {
            total = total + transaction.getAmount();
        }
        if (total >= 0) {
            return "+"+Double.toString(total);
        } else if (total < 0) {
            return ""+Double.toString(total);
        }
        return Double.toString(total);
    }
}
