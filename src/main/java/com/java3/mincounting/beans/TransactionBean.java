package com.java3.mincounting.beans;

import com.java3.mincounting.helpers.Validator;
import com.java3.mincounting.models.Transaction;
import com.java3.mincounting.models.TransactionDatastore;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.bean.ManagedBean;
import jakarta.faces.bean.ManagedProperty;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@ManagedBean(name = "transaction")
@RequestScoped
public class TransactionBean {
    @ManagedProperty(value = "#{param.transactionID}")
    private int objectID = 0;
    @ManagedProperty(value = "#{param.redirect}")
    private String redirect;
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    /*
     * Datastore
     */
    private TransactionDatastore db = new TransactionDatastore();

    /*
     * Transaction Properties
     */
    private String transactionAmount;
    private String transactionDescription;
    private String transactionDate;
    private String transactionID;
    private String projectID;
    private String transactionComment;


    /**
     * Page Constructor
     */
    @PostConstruct
    public void postConstruct() {
        if (objectID != 0) {
            Transaction transaction = db.get(objectID);
            transactionAmount = transaction.getAmount().toString();
            transactionID = transaction.getId().toString();
            transactionDate = transaction.getDate().format(dateFormatter);
            transactionDescription = transaction.getDescription();
            transactionComment = transaction.getComment();
            projectID = transaction.getProjectID().toString();
        }
        if (redirect == null) {
            redirect = "/home.xhtml";
        } else {
            redirect = redirect.replace("%2F", "/");
            redirect = redirect.replace("&#x3F;", "?");
            redirect = redirect.replace("&#x3D;&#xA;;", "=");
        }
    }

    public String createTransaction(String projectID, String date, String amount, String description, String comment) {
        Transaction transaction = new Transaction(Integer.valueOf(projectID), LocalDate.parse(date, dateFormatter), Double.valueOf(amount), description, comment);
        db.create(transaction);
        return redirect + "?faces-redirect=true";
    }

    public String updateTransaction() {
        Transaction transaction = new Transaction(Integer.valueOf(transactionID), Integer.valueOf(projectID), LocalDate.parse(transactionDate, dateFormatter), Double.valueOf(transactionAmount), transactionDescription, transactionComment);
        db.update(transaction);
        return redirect + "?faces-redirect=true";
    }

    public String deleteTransaction(int id, String redirect) {
        db.delete(id);
        return redirect + "?faces-redirect=true";
    }

    public String deleteTransaction(int id) {
        db.delete(id);
        return redirect + "?faces-redirect=true";
    }

    public void validate(FacesContext context, UIComponent comp, Object value) {
        // Rule Factory...
        switch (comp.getId()) {
            case "transactionAmount":
                Validator.required(context, comp, value);
                Validator.isDouble(context, comp, value);
                break;
            case "transactionProjectID":
                Validator.required(context, comp, value);
                Validator.isInteger(context, comp, value);
                break;
            case "transactionDate":
                Validator.required(context, comp, value);
                Validator.isDate(context, comp, value, dateFormatter);
            case "transactionDescription":
                Validator.required(context, comp, value);
                Validator.maxLength(context, comp, value, 250);
                Validator.minLength(context, comp, value, 4);
                break;
            case "transactionComment":
                Validator.required(context, comp, value);
                Validator.maxLength(context, comp, value, 250);
                break;
        }
    }

    /*
     * Getter & Setter
     */
    public List<Transaction> getTransactionList() {
        return db.getAll();
    }

    public List<Transaction> getTransactionList(Boolean type) {
        return db.getAll(type);
    }

    public List<Transaction> getTransactionList(Integer projectID) {
        return db.getAll(projectID);
    }

    public int getObjectID() {
        return objectID;
    }

    public void setObjectID(int objectID) {
        this.objectID = objectID;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getTransactionComment() {
        return transactionComment;
    }

    public void setTransactionComment(String transactionComment) {
        this.transactionComment = transactionComment;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }
}
