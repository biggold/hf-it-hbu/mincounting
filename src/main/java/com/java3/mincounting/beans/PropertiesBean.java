package com.java3.mincounting.beans;

import com.java3.mincounting.models.Menu;
import jakarta.enterprise.context.RequestScoped;
import jakarta.faces.bean.ManagedBean;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "properties")
@RequestScoped
public class PropertiesBean {
    private List<Menu> menu = new ArrayList<Menu>();


    /**
     * Returning Date in specific Format for xHtml
     *
     * @return Current Date as dd.MM.yyyy HH:mm:ss
     */
    public String getCurrentDate() {
        DateFormat dfcurrent = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        return dfcurrent.format(new Date());
    }

    /**
     * Menu function
     *
     * @return Returns a list of menus -> our navigation
     */
    public List<Menu> getMenu() {
        menu.add(new Menu("Dashboard", "/home.xhtml", "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n" +
                "                         fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\"\n" +
                "                         stroke-linejoin=\"round\" class=\"feather feather-home\" aria-hidden=\"true\">\n" +
                "                        <path d=\"M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z\"></path>\n" +
                "                        <polyline points=\"9 22 9 12 15 12 15 22\"></polyline>\n" +
                "                    </svg>"));
        menu.add(new Menu("Projects", "/projects.xhtml", "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-clipboard-data\" viewBox=\"0 0 16 16\">\n" +
                "                        <path d=\"M4 11a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1zm6-4a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7zM7 9a1 1 0 0 1 2 0v3a1 1 0 1 1-2 0V9z\"/>\n" +
                "                        <path d=\"M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z\"/>\n" +
                "                        <path d=\"M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z\"/>\n" +
                "                    </svg>"));
        menu.add(new Menu("Revenues", "/revenues.xhtml", "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\"\n" +
                "                         class=\"bi bi-clipboard-plus\" viewBox=\"0 0 16 16\">\n" +
                "                        <path fill-rule=\"evenodd\"\n" +
                "                              d=\"M8 7a.5.5 0 0 1 .5.5V9H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V10H6a.5.5 0 0 1 0-1h1.5V7.5A.5.5 0 0 1 8 7z\"/>\n" +
                "                        <path d=\"M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z\"/>\n" +
                "                        <path d=\"M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z\"/>\n" +
                "                    </svg>"));
        menu.add(new Menu("Expenses", "/expenses.xhtml", "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\"\n" +
                "                         class=\"bi bi-clipboard-minus\" viewBox=\"0 0 16 16\">\n" +
                "                        <path fill-rule=\"evenodd\" d=\"M5.5 9.5A.5.5 0 0 1 6 9h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z\"/>\n" +
                "                        <path d=\"M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z\"/>\n" +
                "                        <path d=\"M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z\"/>\n" +
                "                    </svg>"));
        return menu;
    }
}
