package com.java3.mincounting.models;

public class Project {
    private Integer id;
    private String name;
    private String description;

    /**
     * Constructor
     *
     * @param id
     * @param name
     * @param description
     */
    public Project(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Project(String name, String description) {
        this.id = null;
        this.name = name;
        this.description = description;
    }

    public Project() {
        this.id = null;
    }

    /*
     * All getters
     */
    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean isNull() {
        return id == null;
    }

    /*
     * All setters
     */
    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
