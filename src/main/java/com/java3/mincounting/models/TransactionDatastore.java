package com.java3.mincounting.models;

import com.java3.mincounting.helpers.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TransactionDatastore implements Datastore<Transaction> {
    private static final String DATA_SOURCE_NAME = "jdbc/simplePool";
    private Connection connection = null;
    private ResultSet rs = null;
    private PreparedStatement stmt = null;
    private String sql;

    @Override
    public Transaction get(int id) {
        sql = "SELECT * FROM simplejdbc.transaction WHERE id = ? LIMIT 1";
        Transaction obj = null;
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            rs.next();
            obj = new Transaction(rs.getInt("id"), rs.getInt("fk_project_id"), rs.getObject("date", LocalDate.class), rs.getDouble("amount"), rs.getString("description"), rs.getString("comment"));
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return obj;
    }

    @Override
    public void create(Transaction obj) {
        sql = "INSERT INTO simplejdbc.transaction (fk_project_id, description, date, amount, type, comment) VALUES (?, ?, ?, ?,?, ?);";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, obj.getProjectID());
            stmt.setString(2, obj.getDescription());
            stmt.setObject(3, obj.getDate());
            stmt.setDouble(4, obj.getAmount());
            stmt.setBoolean(5, obj.getType());
            stmt.setString(6, obj.getComment());
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
    }

    @Override
    public void delete(Transaction obj) {
        delete(obj.getId());
    }

    @Override
    public void delete(int id) {
        sql = "DELETE FROM simplejdbc.transaction WHERE id = ?";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
    }

    @Override
    public List<Transaction> getAll() {
        List<Transaction> objList = new ArrayList<Transaction>();
        sql = "SELECT * FROM simplejdbc.transaction";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Transaction obj = new Transaction(rs.getInt("id"), rs.getInt("fk_project_id"), rs.getObject("date", LocalDate.class), rs.getDouble("amount"), rs.getString("description"), rs.getString("comment"));
                objList.add(obj);
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return objList;
    }

    public List<Transaction> getAll(Boolean type) {
        List<Transaction> objList = new ArrayList<Transaction>();
        sql = "SELECT * FROM simplejdbc.transaction WHERE type=?";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setBoolean(1, type);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Transaction obj = new Transaction(rs.getInt("id"), rs.getInt("fk_project_id"), rs.getObject("date", LocalDate.class), rs.getDouble("amount"), rs.getString("description"), rs.getString("comment"));
                objList.add(obj);
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return objList;
    }

    public List<Transaction> getAll(int projectID) {
        List<Transaction> objList = new ArrayList<Transaction>();
        sql = "SELECT * FROM simplejdbc.transaction WHERE fk_project_id=?";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, projectID);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Transaction obj = new Transaction(rs.getInt("id"), rs.getInt("fk_project_id"), rs.getObject("date", LocalDate.class), rs.getDouble("amount"), rs.getString("description"), rs.getString("comment"));
                objList.add(obj);
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return objList;
    }

    @Override
    public Transaction update(Transaction obj) {
        sql = "UPDATE simplejdbc.transaction SET description=?, date=?, amount=?, type=?, comment=?, fk_project_id=? WHERE id = ?";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, obj.getDescription());
            stmt.setObject(2, obj.getDate());
            stmt.setDouble(3, obj.getAmount());
            stmt.setBoolean(4, obj.getType());
            stmt.setString(5,obj.getComment());
            stmt.setInt(6, obj.getProjectID());
            stmt.setInt(7, obj.getId());
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return this.get(obj.getId());
    }
}
