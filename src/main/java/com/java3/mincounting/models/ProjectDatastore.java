package com.java3.mincounting.models;

import com.java3.mincounting.helpers.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProjectDatastore implements Datastore<Project> {
    private static final String DATA_SOURCE_NAME = "jdbc/simplePool";
    private Connection connection = null;
    private ResultSet rs = null;
    private PreparedStatement stmt = null;
    private String sql;

    @Override
    public Project get(int id) {
        sql = "SELECT * FROM simplejdbc.project WHERE id = ? LIMIT 1";
        Project obj = null;
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                obj = new Project();
            } else {
                obj = new Project(rs.getInt("id"), rs.getString("name"), rs.getString("note"));
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return obj;
    }

    public Project get(String name, String description) {
        sql = "SELECT * FROM simplejdbc.project WHERE name = ? AND note = ? LIMIT 1";
        Project obj = null;
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setString(2, description);
            rs = stmt.executeQuery();
            if (!rs.next()) {
                obj = new Project();
            } else {
                obj = new Project(rs.getInt("id"), rs.getString("name"), rs.getString("note"));
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return obj;
    }

    @Override
    public void create(Project obj) {
        sql = "INSERT INTO simplejdbc.project (name, note) VALUES (?, ?);";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDescription());
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
    }

    @Override
    public void delete(Project obj) {
        delete(obj.getId());
    }

    @Override
    public void delete(int id) {
        sql = "DELETE FROM simplejdbc.project WHERE id = ?";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
    }

    @Override
    public List<Project> getAll() {
        List<Project> objList = new ArrayList<Project>();
        sql = "SELECT * FROM simplejdbc.project";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                Project obj = new Project(rs.getInt("id"), rs.getString("name"), rs.getString("note"));
                objList.add(obj);
            }
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return objList;
    }

    @Override
    public Project update(Project obj) {
        sql = "UPDATE simplejdbc.project SET name=?, note=? WHERE id = ?";
        try {
            connection = Database.open(DATA_SOURCE_NAME);
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDescription());
            stmt.setInt(3, obj.getId());
            int count = stmt.executeUpdate();
        } catch (SQLException var32) {
            var32.printStackTrace();
        } catch (Exception var33) {
            var33.printStackTrace();
        } finally {
            Database.close(rs, stmt, connection);
        }
        return this.get(obj.getId());
    }
}
