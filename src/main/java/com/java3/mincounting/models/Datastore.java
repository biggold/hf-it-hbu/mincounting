package com.java3.mincounting.models;


import java.util.List;

/**
 * Interface describing a datastore
 *
 * @author Cyrill Näf
 */
public interface Datastore<T> {
    /**
     * Get's a specific object from the datastore
     *
     * @param id ID of the object
     * @return The object
     */
    public T get(int id);

    /**
     * Creates a new object in the datastore
     *
     * @param obj A object
     */
    public void create(T obj);

    /**
     * Deletes the object from the datastore
     *
     * @param obj A object
     */
    public void delete(T obj);

    /**
     * Deletes the object from the datastore
     *
     * @param id Objects ID as int
     */
    public void delete(int id);

    /**
     * Returns a List of all objects in the datastore
     *
     * @return List of objects
     */
    public List<T> getAll();

    /**
     * Updates a object
     *
     * @param obj The changed object
     * @return Returns the updated object
     */
    public T update(T obj);

}
