package com.java3.mincounting.models;

import java.time.LocalDate;

public class Transaction {
    private Integer id;
    private Integer projectID;
    private LocalDate date;
    private Double amount;
    private Boolean type;
    private String description;
    private String comment;

    /**
     * Constructor
     */
    public Transaction(Integer id, Integer projectID, LocalDate date, Double amount, String description, String comment) {
        this.id = id;
        this.projectID = projectID;
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.comment = comment;
        this.type = calcType();
    }

    public Transaction(Integer projectID, LocalDate date, Double amount, String description, String comment) {
        this.projectID = projectID;
        this.date = date;
        this.amount = amount;
        this.description = description;
        this.comment = comment;
        this.type = calcType();
    }

    public Transaction() {
        this.id = null;
    }

    private boolean calcType() {
        return amount >= 0;
    }

    /*
     * Getter & Setters
     */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProjectID() {
        return projectID;
    }

    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
        this.type = calcType();
    }

    public Boolean getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
