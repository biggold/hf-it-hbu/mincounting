package com.java3.mincounting.models;

/**
 * Descirbes a navigation menu entry
 */
public class Menu {
    private String action;
    private String svg;
    private String name;

    /**
     * Constructor
     *
     * @param name   The navigations name (display name)
     * @param action The action which should be executed (eg. the reference to the next page)
     * @param svg    The icon which should be displayed (format as svg html tag)
     */
    public Menu(String name, String action, String svg) {
        this.name = name;
        this.action = action;
        this.svg = svg;
    }

    /**
     * Getters
     */
    public String getAction() {
        return action;
    }

    public String getSvg() {
        return svg;
    }

    public String getName() {
        return name;
    }
}
