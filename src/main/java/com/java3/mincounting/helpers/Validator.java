package com.java3.mincounting.helpers;

import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.component.UIInput;
import jakarta.faces.context.FacesContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Validator {
    public static void required(FacesContext context, UIComponent comp, Object value) {
        String tmp = (String) value;
        if (tmp.length() <= 0) {
            ((UIInput) comp).setValid(false);
            context.addMessage(comp.getClientId(context), new FacesMessage("This field is required"));
        }
    }

    public static void maxLength(FacesContext context, UIComponent comp, Object value, int length) {
        String tmp = (String) value;
        if (tmp.length() > length) {
            ((UIInput) comp).setValid(false);
            context.addMessage(comp.getClientId(context), new FacesMessage("The maximum length is " + length));
        }
    }

    public static void minLength(FacesContext context, UIComponent comp, Object value, int length) {
        String tmp = (String) value;
        if (tmp.length() < length) {
            ((UIInput) comp).setValid(false);
            context.addMessage(comp.getClientId(context), new FacesMessage("The minimum length is " + length));
        }
    }

    public static void isInteger(FacesContext context, UIComponent comp, Object value) {
        String tmp = (String) value;
        if (!isValidInteger(tmp)) {
            ((UIInput) comp).setValid(false);
            context.addMessage(comp.getClientId(context), new FacesMessage("Must integer"));
        }
    }

    public static void isDouble(FacesContext context, UIComponent comp, Object value) {
        String tmp = (String) value;
        if (!isValidDouble(tmp)) {
            ((UIInput) comp).setValid(false);
            context.addMessage(comp.getClientId(context), new FacesMessage("Must be a double/decimal"));
        }
    }

    public static void isDate(FacesContext context, UIComponent comp, Object value, DateTimeFormatter dateFormatter) {
        String tmp = (String) value;
        if (!isValidDate(tmp, dateFormatter)) {
            ((UIInput) comp).setValid(false);
            context.addMessage(comp.getClientId(context), new FacesMessage("Date must be in the format dd/mm/YYYY"));
        }
    }


    /*
     * Helpers
     */
    private static boolean isValidDate(String dateStr, DateTimeFormatter dateFormatter) {
        try {
            LocalDate.parse(dateStr, dateFormatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    private static boolean isValidDouble(String dateStr) {
        try {
            Double.valueOf(dateStr);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean isValidInteger(String dateStr) {
        try {
            Integer.valueOf(dateStr);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
