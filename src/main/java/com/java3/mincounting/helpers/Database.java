package com.java3.mincounting.helpers;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.*;

public class Database {
    /**
     * Generates a new database object out of the datasource
     *
     * @param source
     * @return
     * @throws Exception
     */
    public static Connection open(String source) throws Exception {
        InitialContext context = new InitialContext();
        DataSource ds = (DataSource) context.lookup(source);
        return ds.getConnection();
    }


    /**
     * Helper to close all the db stuff
     *
     * @param rs         ResultSet
     * @param stmt       Statement
     * @param connection Connection
     * @throws Exception Pass every exception back to caller if not already handled
     */
    public static void close(ResultSet rs, Statement stmt, Connection connection) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException var30) {
                var30.printStackTrace();
            }
        }
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException var31) {
                var31.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException var29) {
                var29.printStackTrace();
            }
        }
    }
}
