# MinCounting
The minimalistic accounting application based on Java with JSF.

# Impressions
## Dashboard
![Dashboard / Home Screen][dashboard]

## Dashboard Detail
![Dashboard Detail / Project Summary][dashboard-detail]

## Projects
![Projects View][projects]

## Create Transaction
![Create new Transaction][create-transaction]

# Installation
You need the following to get the app flying.

## Application Server
A glassfish application server with propper configuration!

## Database
A database under is required from your application server (glassfish 6.0) as jdbc driver.
The name should be: jdbc/simplePool

## Database Schema
Import the database schema from the file "create_tables.sql"

## Lunch the application
Now you can lunch the application. To get a minimal data set you can import the "sample-import.xml" file!





[dashboard]: docs/images/dashboard.png
[dashboard-detail]: docs/images/dashboard-detail.png
[projects]: docs/images/projects.png
[create-transaction]: docs/images/create-transaction.png